<?php
namespace Dayone\Issuer;

class Agoda {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {
        \App::register('Dayone\Issuer\AgodaServiceProvider');
        return 'Agoda::index';
    }
}