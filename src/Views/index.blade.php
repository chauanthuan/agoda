@extends('layouts.voucher')

@section('content')
<style>

</style>
<?php //dd($voucher);?>
	<?php //$vc_gotit = false;?>
	<div id="voucher_wrapper" class="techcombank">
		<div class="voucher_header">
			<a class="logo" href="{{env('WEB_LINK')}}"><img src="../layouts/v2/images/mvoucher/logo_header.png" ></a>
			<a href="{{env('WEB_LINK')}}">{{trans('content.what_gotit')}}</a>
			<div class="language_fs">
				<?php
				$browserLang = Request::server('HTTP_ACCEPT_LANGUAGE');
				$langPos = strpos($browserLang, 'vi');

				if(!$langPos){
					$browserLang = 'en';
				}else{
					$browserLang = 'vi';
				}

				$lang = Cookie::get('laravel_language', $browserLang);
				?>
				@if( $lang == "vi")
					<p><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/></p>
				@else
					<p><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/></p>
				@endif
				<div class="drop_language" style="display:none;">
					<a href="javascript:void(0)" class="" data-lang="vi"><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/>VI</a>
					<a href="javascript:void(0)" class="" data-lang="en"><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/>EN</a>
				</div>
			</div>
		</div>
		<div class="voucher_body" style="position:relative">
			<!-- Check if is Normal voucher -->
			@if(!$vc_gotit)
				@if($voucher_tp != null)
					<div class="vc_template" style="">
						<img src="{{env('IMG_SERVER').env('AWS_VOUCHER_TEMPLATE_FOLDER').'/'.$voucher_tp->top_image}}" height="" width="100%" style="margin:0 auto !important;display:block;">
					</div>
				@endif
				<div class="voucher_info">
					<div class="img">
						@if($voucher->state == 4)
							<p style="position:absolute;
										top:10px;
										right:10px;
										padding:5px 10px;
										background:#ff9500;
										color:#fff;
										font-family: Roboto;
										font-size: 14px;
										font-weight:bold;
										font-size:18px;
										z-index:9;
										border-radius:4px;
										">
								{{trans('content.used')}}</p>
						@endif
						@if($voucher->state == 8)
							<p style="position:absolute;
								top:10px;
								right:10px;
								padding:5px 10px;
								background:#FF5F5F;
								color:#fff;
								z-index:9;
								font-family: Roboto;
								font-size: 14px;
								font-weight: bold;
								text-align: right;
								color: #ffffff;
								border-radius:4px;
								">
								{{trans('content.v_expired')}}</p>
						@endif
						<img src="{{ Image::show($voucher->img_path)  }}">
					</div>
					<div class="cl"></div>
					<div class="product_info">

						<div class="detail">
							<div class="brand_logo">
								<img src="{{ Image::show($product->brand->logo_img->img_path) }}" alt="">
							</div>
							<!-- <h3>{{$voucher->brand_name}}</h3> -->
							<h3>{{ Translate::transObj($product, 'name')   }}</h3>
							@if($psize != null)
								<p>{{$psize}}</p>
							@endif
						</div>
						<div class="cl"></div>
						<div class="barcode">
							<?php //dd($voucher)?>

							@if($disableBarcode)
							<p class="barcode disable">
								<span>{{ trans('content.voucher_fb')}}</span>
								@if(!isset($loginUrl))
								<a class="loginfb_btn" href="javascript:void(0)" onClick="loginFb();"><i class="fa fa-facebook-square"></i>Login Facebook</a>
								@else
								<a href="{{ $loginUrl }}">
									<img src="../layouts/v2/images/voucher/fblogin-button.png" alt="" style="opacity: 1;">
								</a>
								@endif
							</p>
							@else
							<p style="font-size:19px;margin-top:5px;">Mã code: <b>5WZiiF</b></p>
							<p>{{ trans('content.v_validity').": ".date('d/m/Y', strtotime($voucher->expired_date))}}</p>
							<div class="agoda_wrap">
								<div class="agoda_top">
									<div class="image">
										<!-- <img src="../layouts/v2/images/agoda_step.png"> -->
									</div>
									<div class="text">
										<p>1. Tìm khách sạn <br>trên agoda.vn</p>
										<p>2. Chọn khách sạn <br>có "khuyến mãi hợp lệ"</p>
										<p>3. Nhập mã code <br>khuyến mãi</p>
									</div>
								</div>
								<div class="agoda_bottom">
									<div class="agd_bottom_left">
										<img src="../layouts/v2/images/agoda_logo.png">
									</div>
									<div class="agd_bottom_right">
										<h3>Sử dụng ngay</h3>
										<p>Click vào <a href="https://www.agoda.com">agoda.com</a></p>
									</div>
								</div>
							</div>
							@endif
						</div>
					</div>
				</div>
				
		@else
			<!-- Check if is gotit voucher -->
				<div class="voucher_info gotit_voucher_n">
					@if($voucher_tp != null)
						<div class="vc_template" style="">
							<img src="{{env('IMG_SERVER').env('AWS_VOUCHER_TEMPLATE_FOLDER').'/'.$voucher_tp->top_image}}" height="" width="100%" style="margin:0 auto !important;display:block;">
						</div>
					@endif
					<div class="img">
						@if($voucher->state == 4)
							<p style="position:absolute;
										top:10px;
										right:10px;
										padding:5px 10px;
										background:#ff9500;
										color:#fff;
										font-weight:bold;
										font-size:14px;
										border-radius:4px;
										z-index:9;
										">
								{{trans('content.used')}}</p>
						@endif
						@if($voucher->state == 8)
							<p style="position:absolute;
								top:10px;
								right:10px;
								padding:5px 10px;
								background:#FF5F5F;
								color:#fff;
								font-weight:bold;
								font-size:14px;
								border-radius:4px;
								z-index:9;
								">
								{{trans('content.v_expired')}}</p>
					@endif
					<!-- <img src="../layouts/v2/images/mvoucher/gotit_voucher.png"> -->
						<img src="{{ Image::show($voucher->img_path)  }}">
						
					</div>
					<?php $code =base64_encode(base64_encode($voucher->code."@@hoayeuthuong"));?>
					<div class="all_brand_n">
						<!-- <p>{{ trans('content.redeemalble_store',['Num_store'=>$num_brand->total])}}</p> -->
						@if($list_brand != "")
							<div class="list">
								<div class="wrap_img">
									@foreach($list_brand as $brand)
										<img class="" src="{{ Image::show($brand->img_path) }}" data-id="{{$brand->brand_id}}"/>
									@endforeach
								</div>
							</div>
					@endif
					<!-- <img src="../layouts/v2/images/mvoucher/all_brand.png"> -->
					</div>
					<!-- <h3 class="see_all_brand">{{ trans('content.see_all_brand')}}</h3> -->
					<div class="cl"></div>
					<div class="product_info">

						<div class="detail">
							<!-- <div class="brand_logo">
								<img src="{{ Image::show($product->brand->logo_img->img_path) }}" alt="">
							</div> -->
							<!-- <h3>{{$voucher->brand_name}}</h3> -->
							<h3>{{ Translate::transObj($product, 'name')   }}</h3>
							@if($psize != null)
								<p>{{$psize}}</p>
							@endif
						</div>
						<div class="cl"></div>
						<div class="barcode">
							<?php //dd($voucher)?>

							@if($disableBarcode)
							<p class="barcode disable">
								<span>{{ trans('content.voucher_fb')}}</span>
								@if(!isset($loginUrl))
								<a class="loginfb_btn" href="javascript:void(0)" onClick="loginFb();"><i class="fa fa-facebook-square"></i>Login Facebook</a>
								@else
								<a href="{{ $loginUrl }}">
									<img src="../layouts/v2/images/voucher/fblogin-button.png" alt="" style="opacity: 1;">
								</a>
								@endif
							</p>
							@else
								<p style="font-size:19px;margin-top:5px;">Mã code: <b>5WZiiF</b></p>
								<p>{{ trans('content.v_validity').": ".date('d/m/Y', strtotime($voucher->expired_date))}}</p>
								<div class="agoda_wrap">
									<div class="agoda_top">
										<div class="image">
											<!-- <img src="../layouts/v2/images/agoda_step.png"> -->
										</div>
										<div class="text">
											<p>1. Tìm khách sạn <br>trên agoda.vn</p>
											<p>2. Chọn khách sạn <br>có "khuyến mãi hợp lệ"</p>
											<p>3. Nhập mã code <br>khuyến mãi</p>
										</div>
									</div>
									<div class="agoda_bottom">
										<div class="agd_bottom_left">
											<img src="../layouts/v2/images/agoda_logo.png">
										</div>
										<div class="agd_bottom_right">
											<h3>Sử dụng ngay</h3>
											<p>Click vào <a href="https://www.agoda.com">agoda.com</a></p>
										</div>
									</div>
								</div>
							@endif
						</div>
					</div>
				</div>
		@endif
		<!-- End voucher info -->


			<input type="hidden" name="product_id" value="{{$voucher->product_id}}">
			<input type="hidden" name="price_id" value="{{$voucher->price_id}}">
			<div class="acor-info-vc voucher_note">
				<ul>
					<li>
						<h2>{{ trans('content.product_description') }}</h2>
						<div>
							<p>{!! Translate::transObj($product, 'desc') !!}</p>
						<!-- <h4>{{ trans('content.how_it_work') }}</h4>
		                        <p>{!! Translate::transObj($product, 'service_guide') !!}</p>
		                        <br>
		                        <h4>{{ trans('content.brand') }} : <a href="/brand/{{ $product->brand->name_slug }}">{{ Translate::transObj($product->brand, 'name') }}</a></h4>
		                        <p>{{ trans('content.phone') }} : {{ $product->brand->phone }}</p>
		                        <p>{{ trans('content.address') }} : {{ Translate::transObj($product->brand, 'address') }}</p> -->

							<h3 class="viewMap find_store"><img src="/layouts/v2/images/mvoucher/map_icon.png" height="20px"><a href="#" class="list_btn" data-brandid="{{$voucher->brand_id}}" data-storegroupid ="{{ $product->store_group_id }}">{{ trans('content.find_near')}}</a> &nbsp;</h3>
						</div>
					</li>
				<!-- <li>
							<h2 class="viewMap"><a href="javascript:void(0)">{{ trans('content.show_store')}}</a></h2>
						</li> -->
					<li>
						<h2>{{ trans('content.important')}}</h2>
						<div>
							<p>{{ trans('content.important_text')}}</p>
						</div>
					</li>
					<li>
						<h2>{{ trans('content.term_and_condition') }}</h2>
						<div>
							<p>{!! Translate::transObj($product, 'note') !!}</p>
						</div>
					</li>
					
				</ul>
			</div>

			<div class="send_again">
				<p>{{ trans('content.save_use_late') }}</p>
				{{--<a class="save_img" href="@if(isset($disableBarcode) && $disableBarcode == false) {{ env('IMG_SERVER').env('AWS_VOUCHER_FOLDER').'/'.$link.'-'.md5($voucher->voucher_id.$voucher->code).'_'.Session::get('laravel_language').'.png' }} @endif" @if(isset($disableBarcode) && $disableBarcode == false) {{ 'download="voucher_'.$link.'"' }} @endif >{{ trans('content.save_voucher') }}</a>--}}
				<a class="save_img" href="@if(isset($disableBarcode) && $disableBarcode == false) {{ route('voucher.save',['code'=>Crypt::encrypt($link.'|'.$voucher->voucher_id.$voucher->code),'lang'=>$lang]) }} @endif" @if(isset($disableBarcode) && $disableBarcode == false) download @endif >{{ trans('content.save_voucher') }}</a>
			</div>
		</div>
		<!-- Google map -->
		<div class="store">
			<div class="map-wrap" id="store_location">
				<div class="map active">
					<div class="voucher_header">
						<a class="logo" href="{{env('WEB_LINK')}}"><img src="../layouts/v2/images/mvoucher/logo_header.png" ></a>
						<a href="{{env('WEB_LINK')}}">{{trans('content.what_gotit')}}</a>
						<div class="language_fs">
							<?php
							$browserLang = Request::server('HTTP_ACCEPT_LANGUAGE');
							$langPos = strpos($browserLang, 'vi');

							if(!$langPos){
							$browserLang = 'en';
							}else{
							$browserLang = 'vi';
							}

							$lang = Cookie::get('laravel_language', $browserLang);
							?>
							@if( $lang == "vi")
								<p><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/></p>
							@else
								<p><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/></p>
							@endif
							<div class="drop_language" style="display:none;">
								<a href="javascript:void(0)" class="" data-lang="vi"><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/>VI</a>
								<a href="javascript:void(0)" class="" data-lang="en"><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/>EN</a>
							</div>
						</div>
					</div>
					<div class="top_map">
						<h3>
							<img src="{{ Image::show($product->brand->logo_img->img_path) }}" alt="">
							<?php echo mb_strimwidth($voucher->brand_name, 0, 25, "...", 'UTF-8');?>
							{{--$voucher->brand_name--}}
						</h3>
						<span class="back_voucher"></span>
						@if($vc_gotit)
							<span class="show_more_brand"></span>
						@endif
					</div>
					<div class="find_store">
						<input name="brand_id_choose" type="hidden" value="">
						<h3><img src="/layouts/v2/images/mvoucher/map_icon.png" height="20px"><a href="#" class="find" data-brandid="{{$voucher->brand_id}}" data-storegroupid ="{{ $product->store_group_id }}">{{ trans('content.find_near')}}</a></h3>
					</div>
					<div id="embed-map"></div>
					<div class="add-wrap">
						<div class="list-add">
							<input type="hidden" name="store_group_id" value="{{ $product->store_group_id }}">
							<ul>
								<?php
								$number = 0;
								?>
								@foreach($stores as $store)
									<?php //dd($store->address_vi)?>
									<li>
										<p class="goto-location" data-number="<?php echo $number ?>" style="cursor: pointer;">
											{{ Translate::transObj($store, 'brand_name') . " - " . Translate::transObj($store, 'name') }}<br/>
											<span>{{ Translate::transObj($store, 'address') }}</span><br>
											<span>{{ trans('content.phone')}}: {{$store->phone}}</span>
										<?php $browser = strtolower($_SERVER['HTTP_USER_AGENT']);?>
										@if(stripos($browser,'iphone') !== false || stripos($browser,'ipad') !== false)
											<!-- <a href="comgooglemaps://?q={{Translate::transObj($store,'address')}}&center={{$store->lat}},{{$store->long}}"> -->
												<a href="comgooglemaps://?daddr={{Translate::transObj($store,'address')}}&directionsmode=driving">
												@else
													<!-- <a href="geo:{{$store->lat}},{{$store->long}}?q={{Translate::transObj($store,'address')}}"> -->
														<a href="google.navigation:q={{Translate::transObj($store,'address')}}&{{$store->lat}},{{$store->long}}">
															@endif
															<img src="../layouts/v2/images/mvoucher/direct.png"></a>
										</p>
									</li>
									<?php
									$number++;
									?>
								@endforeach
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>

		
		<div class="brand_ls_voucher active">
			<div class="voucher_header">
				<a class="logo" href="{{env('WEB_LINK')}}"><img src="../layouts/v2/images/mvoucher/logo_header.png"></a>
				<a href="{{env('WEB_LINK')}}">{{trans('content.what_gotit')}}</a>
				<div class="language_fs">
					<?php
					$browserLang = Request::server('HTTP_ACCEPT_LANGUAGE');
					$langPos = strpos($browserLang, 'vi');

					if(!$langPos){
					$browserLang = 'en';
					}else{
					$browserLang = 'vi';
					}

					$lang = Cookie::get('laravel_language', $browserLang);
					?>
					@if( $lang == "vi")
						<p><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/></p>
					@else
						<p><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/></p>
					@endif
					<div class="drop_language" style="display:none;">
						<a href="javascript:void(0)" class="" data-lang="vi"><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/>VI</a>
						<a href="javascript:void(0)" class="" data-lang="en"><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/>EN</a>
					</div>
				</div>
			</div>
			<div class="top_brand">
				<h3>{{ trans('content.all_brand')}}</h3>
				<span class="back_store"></span>
			</div>
			<div class="band_list">
				<p>{{ trans('content.choose_brand')}}</p>
				@if($list_brand != "")
					<input type="hidden" name="store_group_id" value="{{$product->store_group_id}}">
					@foreach($list_brand as $brand)
						<div class="brand_item">
							<a href="javascript:void(0)" data-id = "{{$brand->brand_id}}"><img class="logo-brand" src="{{ Image::show($brand->img_path) }}"/></a>
						</div>
					@endforeach
				@else
					<div class="brand_item">
						<a href="javascript:void(0)" data-id = "{{$voucher->brand_id}}"><img class="logo-brand" src="{{ Image::show($voucher->brand_img_path) }}"/></a>
					</div>
				@endif
			</div>
		</div>

		<div class="voucher_footer">
			@if($voucher_tp != null)
				<div class="vc_template_foot" style="">
					<img src="{{env('IMG_SERVER').env('AWS_VOUCHER_TEMPLATE_FOLDER').'/'.$voucher_tp->bottom_image}}" height="" width="100%" style="margin:0 auto !important; display:block;">
				</div>
			@endif
			<div class="footer_info">
				<div class="pull-left">
					<a class="logo_footer" href="{{env('WEB_LINK')}}"><img src="../layouts/v2/images/mvoucher/logo_header.png" height="32px"></a>
				</div>
				<div class="pull-right">
					<a href="{{env('WEB_LINK')}}">www.gotit.vn</a> &nbsp;|&nbsp; 1900 5588 20
				</div>
			</div>
		</div>
		<input name="img_server" type="hidden" value="{{ env('IMG_SERVER')}}">
	</div>

<style type="text/css">
	#voucher_wrapper .voucher_body .voucher_info .product_info{
		max-width: 100%;
	}
	#voucher_wrapper .voucher_body .voucher_info .product_info .detail{
		max-width: 260px;
		margin: 0 auto;
		display: block;
		float: none;
	}
	.agoda_wrap{
		width: 100%;
		margin-top: 10px;
	}
	.agoda_wrap .agoda_top{
		display: block;
		overflow: hidden;
	}
	.agoda_wrap .agoda_top .image{
		height: 75px;
		width: 100%;
		background: url(../layouts/v2/images/agoda_step.png) no-repeat  center 40% #fafafa;
		background-size: 90% auto;
		margin-bottom: 5px;
	}
	.agoda_wrap .agoda_top .image img{
		width: 100%;
	}
	.agoda_wrap .agoda_top .text{
		margin: 0 auto;
		max-width: 335px;
	}
	.agoda_wrap .agoda_top .text p{
		width: 33.3%;
		display: inline-block;
		float: left;
		text-align: center;
		font-size: 11px !important;
		color: #333;
	}
	.agoda_wrap .agoda_bottom{
		max-width: 280px;
		margin: 0 auto;
		padding-top: 20px;
		display: block;
		overflow: hidden;
	}
	.agoda_wrap .agoda_bottom .agd_bottom_left{
		width: 45%;
		float: left;
	}
	.agoda_wrap .agoda_bottom .agd_bottom_right{
		width: 55%;
		float: left;
	}
	.agoda_wrap .agoda_bottom .agd_bottom_left img{
		width: 100%;
		max-width: 115px;
	}
	.agoda_wrap .agoda_bottom .agd_bottom_right{
		text-align: left;
	}
	.agoda_wrap .agoda_bottom .agd_bottom_right h3{
		text-transform: uppercase;
		margin-top: 8px;
		font-size: 16px;
		padding-left: 5px;
	}
	.agoda_wrap .agoda_bottom .agd_bottom_right p{
		padding-left: 5px;
	}
	@media(max-width: 375px){
		.agoda_wrap .agoda_top .image {
		    height: 75px;
		    width: 100%;
		    background: url(../layouts/v2/images/agoda_step.png) no-repeat center 40% #fafafa;
		    background-size: 95% auto;
		    margin-bottom: 5px;
		}
	}
	@media(max-width: 320px){
		.agoda_wrap .agoda_top .text{
			max-width: 100%;
		}
		.agoda_wrap .agoda_top .text p{
			font-size: 10px !important;
		}
		.agoda_wrap .agoda_top .image {
		    height: 75px;
		    width: 100%;
		    background: url(../layouts/v2/images/agoda_step.png) no-repeat center 40% #fafafa;
		    background-size: 100% auto;
		    margin-bottom: 5px;
		}
	}
</style>


	<script type="text/javascript">
		<?php
		$googleMaps = array();
		foreach ($stores as $store) {
		$googleMaps[] = array(
		'name'  => Translate::transObj($store, 'name'),
		'brand_name'  => Translate::transObj($store, 'brand_name'),
		'lat'   => $store->lat,
		'lng'   => $store->long,
		'address'   =>  Translate::transObj($store, 'address'),
		'phone' => $store->phone
		);
		}
		echo 'var mapStores = '.json_encode($googleMaps);
		?>
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjbeyHo86kWXc3nDZYK8q4AW5Joi0mvOY&v=3.exp&libraries=places"></script>

@endsection